# IronWorks

## Project Description

__IronWorks__ is a collection of libraries and userspace tools for working with
the files and data streams of FINAL FANTASY XIV: A Realm Reborn.

## Libraries

### IronWorks.Kaitai

A low-level parser (used by IronWorks.Parser) that produces a concrete syntax
tree (a.k.a. parse tree) from the raw files and data streams of FINAL FANTASY
XIV: A Realm Reborn.

### IronWorks.Monads

A small library of monads used in various parts of IronWorks.

### IronWorks.Parser

A collection of parsers that produce IronWorks.Structures concrete data
structures from the raw files and data streams of FINAL FANTASY XIV: A Realm
Reborn.

### IronWorks.Structures

A collection of concrete data structures (both mutable and immutable) for
working with the data of FINAL FANTASY XIV: A Realm Reborn.

## Userspace Utilities

### Biggs

A traditional \*NIX style command-line tool for exploring and extracting data
from SqPack format archives.

## License

__IronWorks__ is licensed under the terms of the [MIT License](LICENSE.md).
